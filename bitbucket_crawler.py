"""Module for "crawling" API resources in Bitbucket Cloud."""

import random
import requests

from datetime import datetime
from urllib.parse import urljoin


class Crawler(object):
    """Class responsible for exploring API resources for a given workspace.

    The way this class works is by starting with the list of repositories in
    the workspace, then expanding the list of URLs to hit over time as it makes
    more requests.
    """

    def __init__(self, credentials=None, workspace_slug=None, endpoint=None):
        """Initialize a Crawler for the given credentials/workspace."""
        self.workspace_slug = workspace_slug or 'atlassian'
        self.endpoint = endpoint or 'https://api.bitbucket.org'
        self.session = requests.Session()
        if credentials is not None:
            self.session.auth = tuple(credentials)

        bootstrap_url = urljoin(
            self.endpoint, '/2.0/repositories/{}'.format(workspace_slug))

        self.links = [Link(bootstrap_url)]

    def crawl(self):
        """Crawl API resources indefinitely."""
        while True:
            yield self.next_response()

    def next_response(self):
        """Make a request to a random API resource.

        This method hits a resource, then explores its "links" property to
        identify other resources for the Crawler to potentially hit on the next
        request.
        """
        link = self.get_next_link()
        response = self.session.get(link.url)
        while not response.ok:
            print('\033[1;30m{} DEAD\033[0m'.format(link.url))
            link.is_dead = True
            link = self.get_next_link()
            response = self.session.get(link.url)
        if not link.crawled:
            self.identify_links(response)
            link.crawled = True
        return response

    def get_next_link(self):
        """Get a link to a random API resource."""
        link = random.choice(self.links)
        while link.is_dead:
            link = random.choice(self.links)
        return link

    def identify_links(self, response):
        """Inspect an API response for links to other API resources.

        This expands the set of URLs that the Crawler knows about and will
        potentially hit for future requests.
        """
        try:
            response_data = response.json()
        except ValueError:
            return
        if not isinstance(response_data, dict):
            return
        if 'next' in response_data:
            self.links.append(Link(response_data['next']))
        if 'values' in response_data:
            self.links += [Link(value['links']['self']['href'])
                           for value in response_data['values']]
        if response_data.get('type') == 'repository':
            self.links += [Link(response_data['links'][link]['href'])
                           for link in ['branches', 'commits', 'source',
                                        'tags']]
        elif response_data.get('type') == 'branch':
            self.links.append(Link(response_data['links']['commits']['href']))
        elif response_data.get('type') == 'commit':
            self.links.append(Link(response_data['links']['diff']['href']))
        elif response_data.get('type') == 'commit_file':
            self.links.append(Link(response_data['links']['history']['href']))


class Link(object):
    """Class representing a link to an API resource.

    This is a glorified wrapper around a URL, with a couple of additional
    attributes to track whether the link has already been explored and whether
    it is "dead" i.e. the last request to the link received a non-2xx response.
    """

    def __init__(self, url):
        """Initialize a Link for the given URL."""
        self.url = url
        self.crawled = False
        self.is_dead = False


if __name__ == '__main__':
    import optparse

    parser = optparse.OptionParser()
    parser.add_option('-a', '--auth', action='store', dest='credentials',
                      help='<username>:<password>')
    parser.add_option('-w', '--workspace', action='store',
                      dest='workspace_slug', default='atlassian',
                      help='workspace slug')
    parser.add_option('-e', '--endpoint', action='store', dest='endpoint',
                      default='https://api.bitbucket.org',
                      help='base URL for Bitbucket API')

    options, args = parser.parse_args()
    crawler = Crawler(options.credentials.split(':'), options.workspace_slug,
                      options.endpoint)

    start = datetime.now()
    for resp in crawler.crawl():
        elapsed = datetime.now() - start
        print('GET {} ({:0.2f}s) [{}]'.format(resp.url,
                                              elapsed.total_seconds(),
                                              len(crawler.links)))
